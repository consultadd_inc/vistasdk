.. highlight:: shell

============
Installation
============

At the command line::

    $ easy_install vistasdk

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv vistasdk
    $ pip install vistasdk
