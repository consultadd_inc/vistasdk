===============================
VistaSDK
===============================

.. image:: https://img.shields.io/pypi/v/vistasdk.svg
        :target: https://pypi.python.org/pypi/vistasdk

.. image:: https://img.shields.io/travis/ayushj31/vistasdk.svg
        :target: https://travis-ci.org/ayushj31/vistasdk

.. image:: https://readthedocs.org/projects/vistasdk/badge/?version=latest
        :target: https://readthedocs.org/projects/vistasdk/?badge=latest
        :alt: Documentation Status


vista api client

* Free software: ISC license
* Documentation: https://vistasdk.readthedocs.org.

Features
--------

* TODO

Credits
---------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
