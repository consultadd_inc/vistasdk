import environ

env = environ.Env()

VISTASERVICE_BASE_URL = env('VISTASERVICE_BASE_URL', default="tixdovistanew-dev.ap-south-1.elasticbeanstalk.com")
# VISTASERVICE_BASE_URL = env('VISTASERVICE_BASE_URL', default="ec2-35-154-64-226.ap-south-1.compute.amazonaws.com")
# VISTASERVICE_BASE_URL = env('VISTASERVICE_BASE_URL', default="tixdovista-dev-2.ap-southeast-1.elasticbeanstalk.com")
# VISTASERVICE_BASE_URL = "192.168.3.57:45845"

VISTASERVICE_BASE_PROTOCOL = "http"

service_definitions = {

    "VistaService": {
        "resources": {

            "seats": {
                "endpoint": "/getseatlayout/",
                "required_params": [],
                "optional_params": [],
            },
            "lockseats": {
                "endpoint": "/lockseats/",
                "required_params": [],
                "optional_params": [],
            },
            "bookseats": {
                "endpoint": "/bookseats/",
                "required_params": [],
                "optional_params": [],
            },
            "canceltransaction": {
                "endpoint": "/canceltransaction/",
                "required_params": [],
                "optional_params": [],
            },
            "continuetransaction": {
                "endpoint": "/continuetransaction/",
                "required_params": [],
                "optional_params": [],
            },
            "cancelbook": {
                "endpoint": "/cancelbook/",
                "required_params": [],
                "optional_params": [],
            },
            "bookingstatus": {
                "endpoint": "/getbookingstatus/",
                "required_params": ['cinema_code', 'booking_id'],
                "optional_params": [],
            },
            "activemovies": {
                "endpoint": "/activemovies/",
                "required_params": [],
                "optional_params": [],
            },

            "activeshows": {
                "endpoint": "/activeshows/",
                "required_params": [],
                "optional_params": [],
            },
            "allmovieshows": {
                "endpoint": "/allmovieshows/",
                "required_params": [],
                "optional_params": [],
            },

            "movieshows": {
                "endpoint": "/movietheatreshows/",
                "required_params": [
                    "cinema_code", "movie_code"
                ],
                "optional_params": [],
            },

            "theatreshows": {
                "endpoint": "/theatreshows/",
                "required_params": [
                    "cinema_code"
                ],
                "optional_params": [],
            },

            "showprice": {
                "endpoint": "/showprice/",
                "required_params": [
                    "cinema_code", "priceGroup_code"
                ],
                "optional_params": [],
            },

            "showlanguage": {
                "endpoint": "/showlanguage/",
                "required_params": [
                    "cinema_code", "show_code"
                ],
                "optional_params": [],
            },

            "movies": {
                "endpoint": "/getmovies/",
                "required_params": [],
                "optional_params": [],
            },
            "theatres": {
                "endpoint": "/gettheatres/",
                "required_params": [],
                "optional_params": [],
            },
            "states": {
                "endpoint": "/getshows/",
                "required_params": ["name"],
                "optional_params": [],
            },
            "screens": {
                "endpoint": "/screens/",
                "required_params": [],
                "optional_params": [],
            },
            "synccinemadata": {
                "endpoint": "/synccinemadata/",
                "required_params": [],
                "optional_params": [],
            },

            # Cinepolis

            "getcinepolislayout": {
                "endpoint": "/getcinepolislayout/",
                "required_params": ['cinema_code', 'session_id'],
                "optional_params": [],
            },
            "cinepolislockseats": {
                "endpoint": "/cinepolislockseats/",
                "required_params": ['cinema_code', 'session_id', 'temp_trans_id', 'ticket_type', 'no_of_tickets',
                                    'selected_seats'],
                "optional_params": [],
            },
            "cinepolisbookseats": {
                "endpoint": "/cinepolisbookseats/",
                "required_params": ['cinema_code', 'temp_trans_id', 'payment_string', 'customer_details', 'comments'],
                "optional_params": [],
            },
            "cinepoliscanceltrans": {
                "endpoint": "/cinepoliscanceltrans/",
                "required_params": ['cinema_code', 'temp_trans_id'],
                "optional_params": [],
            },
            "cinepoliscontinuetrans": {
                "endpoint": "/cinepoliscontinuetrans/",
                "required_params": ['cinema_code', 'temp_trans_id'],
                "optional_params": [],
            },
            "cinepolisbookingstatus": {
                "endpoint": "/cinepolisbookingstatus/",
                "required_params": ['cinema_code', 'booking_id'],
                "optional_params": [],
            },

            # Carnival

            "carnivallayout": {
                "endpoint": "/carnivallayout/",
                "required_params": ['cinema_code', 'session_id'],
                "optional_params": [],
            },
            "carnivallockseats": {
                "endpoint": "/carnivallockseats/",
                "required_params": ['cinema_code', 'session_id', 'temp_trans_id', 'ticket_type', 'no_of_tickets',
                                    'selected_seats'],
                "optional_params": [],
            },
            "carnivalbookseats": {
                "endpoint": "/carnivalbookseats/",
                "required_params": ['cinema_code', 'temp_trans_id', 'payment_string', 'customer_details', 'comments'],
                "optional_params": [],
            },
            "carnivalcanceltrans": {
                "endpoint": "/carnivalcanceltrans/",
                "required_params": ['cinema_code', 'temp_trans_id'],
                "optional_params": [],
            },
            "carnivalcontinuetrans": {
                "endpoint": "/carnivalcontinuetrans/",
                "required_params": ['cinema_code', 'temp_trans_id'],
                "optional_params": [],
            },
            "carnivalbookingstatus": {
                "endpoint": "/carnivalbookingstatus/",
                "required_params": ['cinema_code', 'booking_id'],
                "optional_params": [],
            },

            # ShowBizz Schedule Services

            "showbiz_schedule_city": {
                "endpoint": "/ShowBizzScheduleCity/",
                "required_params": ['CityId'],
                "optional_params": [],
            },

            "showbiz_schedule_city_date": {
                "endpoint": "/ShowBizzScheduleCityDate/",
                "required_params": ['CityId', 'ShowDate'],
                "optional_params": [],
            },

            "showbiz_schedule_city_movie_date": {
                "endpoint": "/ShowBizzScheduleCityMovieDate/",
                "required_params": ['CityId', 'MovieId', 'ShowDate'],
                "optional_params": [],
            },

            "showbiz_schedule_city_theatre_movie_date": {
                "endpoint": "/ShowBizzScheduleCityTheatreMovieDate/",
                "required_params": ['CityId', 'TheatreId', 'MovieId', 'ShowDate'],
                "optional_params": [],
            },

            "showbiz_schedule_film": {
                "endpoint": "/ShowBizzScheduleFilm/",
                "required_params": ['FilmId'],
                "optional_params": [],
            },

            "showbiz_schedule_item": {
                "endpoint": "/ShowBizzScheduleItem/",
                "required_params": ['TheatreId', 'MainItemId'],
                "optional_params": [],
            },

            "showbiz_schedule_item_new": {
                "endpoint": "/ShowBizzScheduleItemNew/",
                "required_params": ['TheatreId', 'MainItemId', 'ItemId'],
                "optional_params": [],
            },

            "showbiz_schedule_screen": {
                "endpoint": "/ShowBizzScheduleScreen/",
                "required_params": ['CityId', 'TheatreId', 'ScreenId'],
                "optional_params": [],
            },

            "showbiz_schedule_sold_status": {
                "endpoint": "/ShowBizzScheduleSoldStatus/",
                "required_params": ['TheatreId', 'PartnerId', 'PartnerPwd', 'UniqueRequestId'],
                "optional_params": [],
            },

            "showbiz_schedule_theatre": {
                "endpoint": "/ShowBizzScheduleTheatre/",
                "required_params": ['CityId', 'TheatreId'],
                "optional_params": [],
            },

            "showbiz_schedule_theatre_date": {
                "endpoint": "/ShowBizzScheduleTheatreDate/",
                "required_params": ['TheatreId', 'MovieId', 'ShowDate'],
                "optional_params": [],
            },

            "showbiz_available_seats": {
                "endpoint": "/showbizzavailableseats/",
                "required_params": ['PartnerId', 'PartnerPwd', 'TheatreId', 'ScreenId', 'MovieId', 'ShowDate'],
                "optional_params": [],
            },

            # ShowBizz Seat Services
            "showbiz_book": {
                "endpoint": "/showbizzbook/",
                "required_params": ['TheatreId', 'BookingId', 'ShowClass', 'NoOfTickets', 'PartnerId', 'PartnerPwd',
                                    'UniqueRequestId', 'ReBook'],
                "optional_params": [],
            },
            "showbiz_book_items": {
                "endpoint": "/showbizzbookitems/",
                "required_params": ['TheatreId', 'PartnerId', 'PartnerPwd', 'UniqueRequestId', 'ItemId1', 'ItemQty1',
                                    'ItemId2', 'ItemQty2', 'ItemId3', 'ItemQty3', 'ItemId4', 'ItemQty4', 'ItemId5',
                                    'ItemQty5', 'ItemId6', 'ItemQty6', 'ItemId7', 'ItemQty7', 'ItemId8', 'ItemQty8',
                                    'ItemId9', 'ItemQty9', 'ItemId10', 'ItemQty10'],
                "optional_params": [],
            },
            "showbiz_book_seats": {
                "endpoint": "/showbizzbookseats/",
                "required_params": ['TheatreId', 'BookingId', 'ShowClass', 'NoOfTickets', 'Seat1', 'Seat2', 'Seat3',
                                    'Seat4', 'Seat5', 'Seat6', 'Seat7', 'Seat8', 'Seat9', 'Seat10', 'PartnerId',
                                    'PartnerPwd', 'UniqueRequestId'],
                "optional_params": [],
            },
            "showbiz_buy": {
                "endpoint": "/showbizzbuy/",
                "required_params": ['TheatreId', 'PartnerId', 'PartnerPwd', 'UniqueRequestId', 'PayType', 'PayConfNo',
                                    'Remarks', 'MobileNo', 'Show_Date', 'Show_Time', 'Screen_Id', 'OrderId', 'OrderRef',
                                    'MTR', 'TransactionId', 'RRN', 'PayAuthId', 'IP'],
                "optional_params": [],
            },
            "showbiz_cancel_book": {
                "endpoint": "/showbizzcancelbook/",
                "required_params": ['TheatreId', 'PartnerId', 'PartnerPwd', 'UniqueRequestId'],
                "optional_params": [],
            },

            "showbiz_show_seats": {
                "endpoint": "/showbizzshowseats/",
                "required_params": ['TheatreId', 'BookingId', 'ShowClass', 'NoOfTickets', 'PartnerId', 'PartnerPwd'],
                "optional_params": [],
            },
            "showbiz_sold_status": {
                "endpoint": "/showbizzsoldstatus/",
                "required_params": ['TheatreId', 'PartnerId', 'PartnerPwd', 'UniqueRequestId', 'ReceiptNumber'],
                "optional_params": [],
            },

            "resource": {
                "endpoint": "/resources/",
                "required_params": [
                    "user", "start_date", "rate", "project"
                ],
                "optional_params": [
                    "end_date", "agreed_hours_per_month",
                ],
            }
        }
    }
}