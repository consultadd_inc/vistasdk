from vistasdk import settings

# from .base_client import ServiceBase
from vistasdk.base_client import ServiceBase

base_url = {
    "url": settings.VISTASERVICE_BASE_URL,
    "protocol": settings.VISTASERVICE_BASE_PROTOCOL,
}

service_definitions = settings.service_definitions


class VistaService(ServiceBase):
    # def __init__(self, token=None, tld=base_url["url"], protocol=base_url["protocol"], auth_data=None):
    # self.token = self._is_logged_in(super(VistaService, self), auth_data)
    #     if self.token is not None:
    #         super(VistaService, self).__init__('VistaService', token, tld, protocol)
    #
    # def _is_logged_in(self, service, auth_data):
    #     email = auth_data["email"]
    #     password = auth_data["password"]
    #     response = service.authenticate(email=email, password=password)
    #     is_logged_in, token = service.login(response)
    #     return token

    def __init__(self):
        super(VistaService, self).__init__('VistaService')

    def get_seatlayout(self, data):
        return self.create("seats", data)

    def lock_seats(self, data):
        return self.create("lockseats", data)

    def book_seats(self, data):
        return self.create("bookseats", data)

    def cancel_transaction(self, data):
        return self.create("canceltransaction", data)

    def continue_transaction(self, data):
        return self.create("continuetransaction", data)

    def cancel_booking(self, data):
        return self.create("cancelbook", data)

    def booking_status(self, data):
        return self.create("bookingstatus", data)

    def get_movies(self):
        return self.list("movies")

    def get_theatres(self):
        return self.list("theatres")

    def get_shows(self):
        return self.list('shows')

    def get_allmovieshows(self):
        return self.list('allmovieshows')

    def get_activemovies(self):
        return self.list("activemovies")

    def get_activeshows(self):
        return self.list("activeshows")

    def get_movieshows(self, data):
        return self.create("movieshows", data)

    def get_theatreshows(self, data):
        return self.create("theatreshows", data)

    def get_showprice(self, data):
        return self.create("showprice", data)

    def get_show_language(self, data):
        return self.create("showlanguage", data)

    def sync_cinema_data(self, data):
        return self.create("synccinemadata", data)

    # Cinepolis Services

    def cinepolis_layout(self, data):
        return self.create("getcinepolislayout", data)

    def cinepolis_lock_seats(self, data):
        return self.create("cinepolislockseats", data)

    def cinepolis_book_seats(self, data):
        return self.create("cinepolisbookseats", data)

    def cinepolis_cancel_trans(self, data):
        return self.create("cinepoliscanceltrans", data)

    def cinepolis_continue_trans(self, data):
        return self.create("cinepoliscontinuetrans", data)

    def cinepolis_booking_status(self, data):
        return self.create("cinepolisbookingstatus", data)

    # Carnival Services

    def carnival_layout(self, data):
        return self.create("carnivallayout", data)

    def carnival_lock_seats(self, data):
        return self.create("carnivallockseats", data)

    def carnival_book_seats(self, data):
        return self.create("carnivalbookseats", data)

    def carnival_cancel_trans(self, data):
        return self.create("carnivalcanceltrans", data)

    def carnival_continue_trans(self, data):
        return self.create("carnivalcontinuetrans", data)

    def carnival_booking_status(self, data):
        return self.create("carnivalbookingstatus", data)

    # ShowBizz Schedule Services
    def showbiz_schedule_city(self, data):
        return self.create("showbiz_schedule_city", data)

    def showbiz_schedule_city_date(self, data):
        return self.create("showbiz_schedule_city_date", data)

    def showbiz_schedule_city_movie_date(self, data):
        return self.create("showbiz_schedule_city_movie_date", data)

    def showbiz_schedule_city_theatre_movie_date(self, data):
        return self.create("showbiz_schedule_city_theatre_movie_date", data)

    def showbiz_schedule_film(self, data):
        return self.create("showbiz_schedule_film", data)

    def showbiz_schedule_item(self, data):
        return self.create("showbiz_schedule_item", data)

    def showbiz_schedule_item_new(self, data):
        return self.create("showbiz_schedule_item_new", data)

    def showbiz_schedule_screen(self, data):
        return self.create("showbiz_schedule_screen", data)

    def showbiz_schedule_sold_status(self, data):
        return self.create("showbiz_schedule_sold_status", data)

    def showbiz_schedule_theatre(self, data):
        return self.create("showbiz_schedule_theatre", data)

    def showbiz_schedule_theatre_date(self, data):
        return self.create("showbiz_schedule_theatre_date", data)

    def showbiz_available_seats(self, data):
        return self.create("showbiz_available_seats", data)

    # ShowBizz Seat Services
    def showbiz_book(self, data):
        return self.create("showbiz_book", data)

    def showbiz_book_items(self, data):
        return self.create("showbiz_book_items", data)

    def showbiz_book_seats(self, data):
        return self.create("showbiz_book_seats", data)

    def showbiz_buy(self, data):
        return self.create("showbiz_buy", data)

    def showbiz_cancel_book(self, data):
        return self.create("showbiz_cancel_book", data)

    def showbiz_show_seats(self, data):
        return self.create("showbiz_show_seats", data)

    def showbiz_sold_status(self, data):
        return self.create("showbiz_sold_status", data)