#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
test_vistasdk
----------------------------------

Tests for `vistasdk` module.
"""

import unittest

from vistasdk import vistasdk


class TestVistasdk(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_000_something(self):
        pass


if __name__ == '__main__':
    import sys
    sys.exit(unittest.main())
